# Python Project 1




## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/jwabend/python-project-1.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/jwabend/python-project-1/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***



## Name
Python lab 1

## Description

Overview: In this week, you have set-up your Python Environment. The Lab for this week demonstrates
your first use of this environment with a fairly simple Python application. You will also use pylint to
verify your code is using professional coding style and standards.
Submission requirements include 2 files. (Zipping them into one file is acceptable):
• Python Voter Registration Application Code (python code)
• Word or PDF file containing your test and pylint results
Python Applications for Lab1: (total 100 points):
This lab consists of two parts. The first exercise produces a voter registration application asking the user
a few simple questions followed by a confirmation of registration, provided the user is eligible. The
second part documents your testing and pylint analysis results.
1. Using your Python programming environment, write a Python application that supports voter
registration. The application will launch and run from the command line prompt. The application will
prompt the user for their first name, last name, age, country of citizenship, state of residence and
zipcode. To be a valid registration all fields must be entered. If they are at least 18 years old and a U.S
citizen, they can move forward and be prompted for the remaining questions and register to vote. If not,
they should not be presented with the additional questions. There should be some error checking logic
on the input statements to make sure the age numbers entered seem reasonable (e.g. a person is
probably not > 120 years) and states should be 2 letters representing only valid U.S. States. The
application should prompt the user for the needed questions to complete the registration and reprompt when data is invalid giving the user the opportunity to retry. The output should summarize the
input data and congratulate the user if they are eligible to vote and entered all of the data. The user
should be given options to exit the program at any time to cancel the registration process.
The following is a possible application interface. Other application interfaces are possible as well. (80
points)
****************************************************************
Welcome to the Python Voter Registration Application.
Do you want to continue with Voter Registration?
Yes.
What is your first name?
Sally
Do you want to continue with the voter Registration?
What is your last name?
Smith
Do you want to continue with the voter Registration?
Yes
What is your age?
49
Do you want to continue with the voter Registration?
2
Yes
Are you a U.S. Citizen?
Yes
Do you want to continue with the voter Registration?
Yes
What state do you live?
MD
Do you want to continue with the voter Registration?
Yes
What is your zipcode?
21012
Thanks for registering to vote. Here is the information we
received:
Name (first last): Sally Smith
Age: 49
U.S. Citizen: Yes
State: MD
Zipcode: 21012
Thanks for trying the Voter Registration Application. Your voter
registration card should be shipped within 3 weeks.
****************************************************************
Hints:
1. Be sure to add logic to test for continuing the registration process.
2. Validate data is valid on entry (e.g. all fields have input data, age seems correct, states seem
correct.)
3. Test with many combinations. For example, what happens if you enter invalid data? Exit the
application at any point, or aren’t 18 years old?
4. Use comments to document your code
5. Use pylint to verify the code style – the goal is a 10!
2. Document your test results for each application within your programming environment. You should
also include and discuss your pylint results for each application. The test document should include a test
table that includes the input values, the expected results and the actual results. A screen capture should
be included that shows the actual test results of running each test case found in the test table. Be sure
to include multiple test cases to provide full coverage for all code. For example, you should demonstrate
each set of logic in the code works as expected and every statement in the code is reached through the
test cases. (20 points)