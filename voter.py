## Author: John Abend
## Date: 3/16/2023
## Class: SDEV 300 7760
## Lab1
## Description: This code is a Python program for a voter registration application.
## The program prompts the user for their personal information, such as their
## name, age, citizenship status, state, and zipcode, and validates the input
## according to specific criteria.
##
## The program also uses regular expressions to ensure that the user's
## name, state abbreviation, and zipcode are correctly formatted.
## Once all of the user's information has been collected and validated,
## the program displays it back to the user and informs them
## that their voter registration card should be shipped within 3 weeks.

"""Voter Registrations"""
import re
from voterhelper import continue_registration

############################################################
###############   Voter Registration Class   ###############
############################################################

class VoterRegistrationApplication:
    """Voter Registation Class"""

    def __init__(self):
        """Self values"""
        #private variables
        self._first_name = None
        self._last_name = None
        self._age = None
        self._us_citizen = None
        self._state = None
        self._zipcode = None

    def run(self):
        """Function that runs Python Voter Registration"""
        print("Welcome to the Python Voter Registration Application.")
        while True:

            # Check if user wants to continue registration
            continue_registration()

            # Prompt user for first name
            self.set_firstname()
            # Validate first name
            if not self.validate_firstname(self._first_name):
                continue

            # Check if user wants to continue registration
            continue_registration()

            # Prompt user for last name
            self.set_lastname()
            # Validate last name
            if not self.validate_lastname(self._last_name):
                continue

            # Check if user wants to continue registration
            continue_registration()

            # Prompt user for age
            self.set_age()
            # Validate _age
            if not self.validate_age(self._age):
                continue

            # Check if user wants to continue registration
            continue_registration()

            # Prompt user for citizenship
            self.set_citizen()
            # Validate citizen
            if not self.validate_us_citizen(self._us_citizen):
                continue

            # Check if user wants to continue registration
            continue_registration()

            # Prompt user for state
            self.set_state()
            # Validate state
            if not self.validate_state(self._state):
                continue

            # Check if user wants to continue registration
            continue_registration()

            # Prompt user for zipcode
            self.set_zipcode()
            # Validate zipcode
            if not self.validate_zipcode(self._zipcode):
                continue

            # Display registered voter's details
            print(
                "\nThanks for registering to vote. Here is the information we received:")
            print("Name (first last):", self._first_name, self._last_name)
            print("Age:", self._age)
            print("U.S. Citizen:", self._us_citizen)
            print("State:", self._state)
            print("Zipcode:", self._zipcode)
            print("\nThanks for trying the Voter Registration Application.")
            print("Your voter registration card should be shipped within 3 weeks.")
            return

    def validate_firstname(self, _name):
        """Validates First Name"""
        while True:
            if not _name:
                print("Please enter a first name.")
                _name = input("What is your first name? ")
            if not re.match(r"^[a-zA-Z]+$", _name):
                print("Name can only contain letters.")
                _name = input("What is your first name? ")
            else:
                self._first_name = _name
                return True

    def validate_lastname(self, _name):
        """Validates Last Name"""
        while True:
            if not _name:
                print("Please enter a last name.")
                _name = input("What is your last name? ")
            if not re.match(r"^[a-zA-Z]+$", _name):
                print("Name can only contain letters.")
                _name = input("What is your last name? ")
            else:
                self._last_name = _name
                return True

    def validate_age(self, _age):
        """Validates Age"""
        while True:
            if not _age:
                print("Please enter an age.")
                _age = input("What is your age? ")
                continue
            try:
                _age = int(_age)
            except ValueError:
                print("Age must be a number.")
                _age = input("What is your age? ")
                continue
            if _age <= 0 or _age > 120:
                print("Age must be between 1 and 120.")
                _age = input("What is your age? ")
                continue
            if _age < 18:
                print("You must be 18 or older to register to vote.")
                _age = input("What is your age? ")
                continue
            self._age = _age
            return True

    def validate_us_citizen(self, _us_citizen):
        """Validates Citizen"""
        while True:
            if not _us_citizen:
                print("Please enter yes or no for U.S. Citizenship.")
            elif _us_citizen == "no":
                print("You must be a U.S. citizen to register to vote.")
            elif _us_citizen == "yes":
                self._us_citizen = _us_citizen
                return True
            else:
                print("Invalid input. Please enter 'yes' or 'no'.")
            _us_citizen = input("Are you a U.S. Citizen? ").lower()

    def validate_state(self, _state):
        """Validates State"""
        while True:
            if not _state:
                print("Please enter a state abbreviation.")
                _state = input("State: ").upper()
                continue
            if not re.match(r"^[A-Z]{2}$", _state):
                print("State abbreviation must be 2 letters.")
                _state = input("Enter state: ").upper()
                continue

            valid__states = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA',
                             'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD',
                             'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ',
                             'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC',
                             'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY']
            if _state in valid__states:
                self._state = _state
                return True

    def validate_zipcode(self, user_zip):
        """Validates Zipcode"""
        while True:
            _zipcode = str(user_zip)
            if not user_zip:
                print("Please enter a zipcode that is 5 digits long: ")
                return False
            if len(user_zip) == 5:
                self._zipcode = _zipcode
                return True

            user_zip = input("Please enter a zipcode that is 5 digits long: ")

    def set_firstname(self):
        """Sets the users first name"""
        self._first_name = input("What is your first name? ")

    def set_lastname(self):
        """Sets the users last name"""
        self._last_name = input("What is your last name? ")

    def set_age(self):
        """Sets the users last age"""
        self._age = input("What is your age? ")

    def set_citizen(self):
        """Sets the users citizenship"""
        self._us_citizen = input("Are you a U.S. Citizen? ").lower()

    def set_state(self):
        """Sets the users state"""
        self._state = input("What state do you live? ").upper()

    def set_zipcode(self):
        """Sets the users zipcode"""
        self._zipcode = input("What is your zipcode? ")
