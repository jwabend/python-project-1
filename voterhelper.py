## Author: John Abend
## Date: 3/16/2023
## Class: SDEV 300 7760
## Lab1
## Description: Voter Helper includes a function called "continue_registration" that runs
## a while loop and prompts the user to input "yes" or "no" to continue or exit the registration
## process. If the user inputs "no", the program prints a thank you message and exits. If the user
## inputs an invalid input, the program prints an error message and continues the loop until a valid
## input is given. The function returns True if the user inputs "yes".

"""Voter Helper"""
import sys

def continue_registration():
    """Asks user if they want to continue"""
    while True:
        continue_prompt = input(
            "Do you want to continue with Voter Registration? ").lower()
        if continue_prompt == "no":
            print("Thanks for trying the Voter Registration Application.")
            sys.exit()
        elif continue_prompt != "yes":
            print("Invalid input. Please enter 'yes' or 'no'.")
            continue
        else:
            return True
        