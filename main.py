## Author: John Abend
## Date: 3/16/2023
## Class: SDEV 300 7760
## Lab1
## Description: Main creates an instance of the VoterRegistrationApplication
## class from the voter module and runs the run() method of the instance. The main() function
## is the starting point of the program and checks if the script is run directly before
## calling the main function.

"""Main"""
import voter

def main():
    """main function"""
    # Create an instance of the VoterRegistrationApplication class
    voter_instance = voter.VoterRegistrationApplication()
    # Call the run method of the voter_instance object
    voter_instance.run()


if __name__ == "__main__":
    # Call the main function if this file is run directly
    main()
